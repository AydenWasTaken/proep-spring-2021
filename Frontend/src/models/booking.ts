import { User } from "./user"
import { Job } from "./job"

export class Booking {
    
      public bookingId!: string;
      public jobId!: string;
      public consumerId!: string;
      public address!: string;
      public city!: string;
      public status!: number;
      public bookingDate!: string;
      public dateCreated!: Date;
      public lastUpdated!: Date;
  
    
  }