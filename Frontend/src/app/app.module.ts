import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ServicesComponent } from './services/services.component';
import { ContactComponent } from './contact/contact.component';
import { AngularFireModule } from '@angular/fire';
import { HttpClientModule } from '@angular/common/http';
import { SocialLoginModule, SocialAuthServiceConfig, SocialAuthService } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { CookieService } from "angular2-cookie/services/cookies.service";
import { JobsPageComponent } from './jobs-page/jobs-page.component';
import { JobComponent } from './job/job.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { ListJobsComponent } from './list-jobs/list-jobs.component';
import { CreateJobComponent } from './create-job/create-job.component';
import { LoginComponent } from './login/login.component';
import { NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilterPipe } from './filter.pipe';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatDatepicker, MatDateRangeInput, MatDateRangePicker } from '@angular/material/datepicker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { TermsComponent } from './terms/terms.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    ServicesComponent,
    ContactComponent,
    JobsPageComponent,
    JobComponent,
    ProfilePageComponent,
    ListJobsComponent,
    CreateJobComponent,
    LoginComponent,
    FilterPipe,
    TermsComponent
  ],
  imports: [
    MatFormFieldModule,MatAutocompleteModule,MatInputModule, MatDatepickerModule,MatNativeDateModule,
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule, ReactiveFormsModule, NgbModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    SocialLoginModule,
    BrowserAnimationsModule,
    FontAwesomeModule
  ],
  providers:
  [
    HttpClientModule,
    HeaderComponent,
    {
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: true,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '829337470495-6mi03de0e2ibj23etrpa1bgdkd5ja5fb.apps.googleusercontent.com'
          )
        },
      ],
    } as SocialAuthServiceConfig
  }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
