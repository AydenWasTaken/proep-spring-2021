import { Pipe, PipeTransform } from '@angular/core';
import { Job } from 'src/models/job';

@Pipe({
  name: 'jobFilter'
})
export class FilterPipe implements PipeTransform {

  transform(jobs: Job[], searchText: string): Job[] {


    if (!jobs){
      return [];
    }
    if (!searchText){
      return jobs;
    }
    searchText = searchText.toLocaleLowerCase();
    return jobs.filter(it =>
      {
        return it.title.toLocaleLowerCase().includes(searchText);
      })
  }

}
