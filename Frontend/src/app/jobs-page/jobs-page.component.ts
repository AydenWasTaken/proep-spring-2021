import { Component, OnInit } from '@angular/core';
import { GetApiService } from '../get-api.service';
import { Job } from '../../models/job';
import { FormControl } from '@angular/forms';



@Component({
  selector: 'app-jobs-page',
  templateUrl: './jobs-page.component.html',
  styleUrls: ['./jobs-page.component.css']
})
export class JobsPageComponent implements OnInit {

  allJobs: Job[] = [];
  queryField: FormControl = new FormControl();

  searchJob!: string;
  constructor(private api: GetApiService) { }

  ngOnInit(): void {
    this.getJobs();
  }

  getJobs(){
    this.api.getJobs().subscribe((jobs)=>{
      this.allJobs = jobs;
      console.log(this.allJobs);
  })
  }
  displayFn(job: Job): string {
    return job!.title;
  }
  
}
