﻿using System;
using System.Threading.Tasks;

using Xunit;

namespace Core.Models.Tests
{
    public class BookingTests : IDisposable
    {
        private readonly Booking booking;

        // setup
        public BookingTests()
        {
            this.booking = new Booking { BookingId = Guid.Empty };
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task Booking_EqualsObject_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            object bookingCheck = new Booking { BookingId = Guid.Empty };

            // Act
            bool result = this.booking.Equals(bookingCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Booking_EqualsObject_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            object bookingCheck = new Booking { BookingId = Guid.NewGuid() };

            // Act
            bool result = this.booking.Equals(bookingCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Booking_EqualsBooking_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.Empty };

            // Act
            bool result = this.booking.Equals(bookingCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Booking_EqualsBooking_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.NewGuid() };

            // Act
            bool result = this.booking.Equals(bookingCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Booking_EqualsOperator_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.Empty };

            // Act
            bool result = this.booking == bookingCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Booking_EqualsOperator_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.NewGuid() };

            // Act
            bool result = this.booking == bookingCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Booking_NotEqualsOperator_ReturnsTrueWhenIdsDoNotMatch()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.NewGuid() };

            // Act
            bool result = this.booking != bookingCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task Booking_NotEqualsOperator_ReturnsFalseWhenIdsMatch()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.Empty };

            // Act
            bool result = this.booking != bookingCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task Booking_GetHashCode_IsEqualWhenIdIsEqual()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.Empty };

            // Act
            int bookingCode = this.booking.GetHashCode();
            int bookingCheckCode = bookingCheck.GetHashCode();

            // Assert
            Assert.Equal(bookingCheckCode, bookingCode);
        }

        [Fact]
        public async Task Booking_GetHashCode_IsDifferentWhenIdIsDifferent()
        {
            // Arrange
            Booking bookingCheck = new Booking { BookingId = Guid.NewGuid() };

            // Act
            int bookingCode = this.booking.GetHashCode();
            int bookingCheckCode = bookingCheck.GetHashCode();

            // Assert
            Assert.NotEqual(bookingCheckCode, bookingCode);
        }
    }
}