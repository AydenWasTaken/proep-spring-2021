﻿using System;
using System.Threading.Tasks;

using Xunit;

namespace Core.Models.Tests
{
    public class UserTests : IDisposable
    {
        private readonly User user;

        // setup
        public UserTests()
        {
            this.user = new User { UserId = "000000000000000000000" };
        }

        // breakdown
        public void Dispose()
        {

        }

        [Fact]
        public async Task User_EqualsObject_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            object userCheck = new User { UserId = "000000000000000000000" };

            // Act
            bool result = this.user.Equals(userCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task User_EqualsObject_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            object userCheck = new User { UserId = "123456789123456789123" };

            // Act
            bool result = this.user.Equals(userCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task User_EqualsUser_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            User userCheck = new User { UserId = "000000000000000000000" };

            // Act
            bool result = this.user.Equals(userCheck);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task User_EqualsUser_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            User userCheck = new User { UserId = "123456789123456789123" };

            // Act
            bool result = this.user.Equals(userCheck);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task User_EqualsOperator_ReturnsTrueWhenIdsMatch()
        {
            // Arrange
            User userCheck = new User { UserId = "000000000000000000000" };

            // Act
            bool result = this.user == userCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task User_EqualsOperator_ReturnsFalseWhenIdsDoNotMatch()
        {
            // Arrange
            User userCheck = new User { UserId = "123456789123456789123" };

            // Act
            bool result = this.user == userCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task User_NotEqualsOperator_ReturnsTrueWhenIdsDoNotMatch()
        {
            // Arrange
            User userCheck = new User { UserId = "123456789123456789123" };

            // Act
            bool result = this.user != userCheck;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task User_NotEqualsOperator_ReturnsFalseWhenIdsMatch()
        {
            // Arrange
            User userCheck = new User { UserId = "000000000000000000000" };

            // Act
            bool result = this.user != userCheck;

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task User_GetHashCode_IsEqualWhenIdIsEqual()
        {
            // Arrange
            User userCheck = new User { UserId = "000000000000000000000" };

            // Act
            int userCode = this.user.GetHashCode();
            int userCheckCode = userCheck.GetHashCode();

            // Assert
            Assert.Equal(userCheckCode, userCode);
        }

        [Fact]
        public async Task User_GetHashCode_IsDifferentWhenIdIsDifferent()
        {
            // Arrange
            User userCheck = new User { UserId = "123456789012345678901" };

            // Act
            int userCode = this.user.GetHashCode();
            int userCheckCode = userCheck.GetHashCode();

            // Assert
            Assert.NotEqual(userCheckCode, userCode);
        }
    }
}