﻿using System;
using System.Collections.Generic;

using Core.Models;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace API.Attributes
{
    /// <summary>
    /// Custom authorization attribute to ensure the user is logged in.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        /// <inheritdoc/>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                User user = (User)context.HttpContext.Items["User"];
                if (user == null)
                    throw new KeyNotFoundException();
            }
            catch (KeyNotFoundException)
            {
                context.Result = new JsonResult("Unauthorized") { StatusCode = StatusCodes.Status401Unauthorized };
            }

        }
    }
}
