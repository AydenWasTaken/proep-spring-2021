﻿using System;

using Core.DataTypes;
using Core.Models;

using Microsoft.EntityFrameworkCore;

namespace API.Db
{
    /// <summary>
    /// The database context wrapper for all tables.
    /// </summary>
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        /// <summary>
        /// The database set for bookings. This is essentially a direct connection to the database.
        /// </summary>
        public DbSet<Booking> Bookings { get; set; }

        /// <summary>
        /// The database set for jobs. This is essentially a direct connection to the database.
        /// </summary>
        public DbSet<Job> Jobs { get; set; }

        /// <summary>
        /// The database set for reviews. This is essentially a direct connection to the database.
        /// </summary>
        public DbSet<Review> Reviews { get; set; }

        /// <summary>
        /// The database set for users. This is essentially a direct connection to the database.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="options"></param>
        public DbContext(DbContextOptions<DbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// This method customizes the behavior when creating an entity. <br></br>
        /// Responsible for ensuring model types are serialized correctly. <br></br>
        /// Responsible for seeding database.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Job>()
                .Property(job => job.JobId)
                .ValueGeneratedNever();

            modelBuilder.Entity<Review>()
                .Property(review => review.ReviewId)
                .ValueGeneratedNever();

            modelBuilder.Entity<User>()
                .Property(user => user.UserId)
                .ValueGeneratedNever();

            modelBuilder.Entity<Booking>()
                .Property(booking => booking.BookingId)
                .ValueGeneratedNever();

            modelBuilder.Entity<Booking>()
                .Property(booking => booking.Status)
                .HasConversion(
                v => v.ToString(),
                v => (STATUS)Enum.Parse(typeof(STATUS), v)
                );

            modelBuilder.Entity<Job>()
                .Property(job => job.LevelOfExperience)
                .HasConversion(
                levelOfExperienceEnum => levelOfExperienceEnum.ToString(),
                levelOfExperienceString => (LEVEL_OF_EXPERIENCE)Enum.Parse(typeof(LEVEL_OF_EXPERIENCE), levelOfExperienceString)
                );

            var userData = new[]
            {
                new
                {
                    UserId = "110811874816751076279",
                    Email = "flambshine0@imgur.com",
                    FirstName = "Frazer",
                    LastName = "Lambshine",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(976) 5971010",
                    Address = "9 Golden Leaf Trail",
                    City = "Balayong",
                    Rating = 2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "110811874816751097523",
                    Email = "sgillivrie1@utexas.edu",
                    FirstName = "Sophie",
                    LastName = "Gillivrie",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1995, 12, 5),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(894) 7687284",
                    Address = "23559 Dexter Center",
                    City = "Barobo",
                    Rating = 1f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "954853874816751076279",
                    Email = "vebbett2@odnoklassniki.ru",
                    FirstName = "Veronike",
                    LastName = "Ebbett",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1950, 2, 26),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(180) 4593004",
                    Address = "056 Springview Terrace",
                    City = "Shchigry",
                    Rating = 3f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "956325694785213654589",
                    Email = "wengel3@diigo.com",
                    FirstName = "Weidar",
                    LastName = "Engel",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1970, 8, 15),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/ff4444/ffffff",
                    Phone = "(282) 5803398",
                    Address = "0 Pierstorff Trail",
                    City = "Kiambu",
                    Rating = 0.2f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "9563256947852136598520",
                    Email = "sdevaney4@histats.com",
                    FirstName = "Shayna",
                    LastName = "Devaney",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2004, 7, 18),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/cc0000/ffffff",
                    Phone = "(971) 6571942",
                    Address = "0 Anhalt Drive",
                    City = "Kudanding",
                    Rating = 4.8f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132544785213654589",
                    Email = "nstrewther5@nsw.gov.au",
                    FirstName = "Norean",
                    LastName = "Strewther",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2001, 3, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(499) 6865801",
                    Address = "30 Cottonwood Drive",
                    City = "Teki Szlacheckie",
                    Rating = 4f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "850132958321456545893",
                    Email = "gdurn6@youku.com",
                    FirstName = "Gertrud",
                    LastName = "Durn",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(1999, 9, 9),
                    ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                    Phone = "(973) 4431447",
                    Address = "1114 Village Green Parkway",
                    City = "Muzhou",
                    Rating = 3.9f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = ""
                },
                new
                {
                    UserId = "118269202694979787959",
                    Email = "nils.moller2000@gmail.com",
                    FirstName = "Nils",
                    LastName = "Möller",
                    Description = "Some longer description showing more detailed information about this user.",
                    DateOfBirth = new DateTime(2000, 5, 2),
                    ProfilePictureLink = "https://lh3.googleusercontent.com/a/AATXAJyaUEquS0bkZVf6rQ7bM4l8dcZ0S9_GUiRYENZKGQ=s96-c",
                    Phone = "+31642868029",
                    Address = "Heezerweg 117, 5614HC",
                    City = "Eindhoven",
                    Rating = 5f,
                    DateCreated = DateTime.Now,
                    LastLogin = DateTime.Now,
                    Token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImQzZmZiYjhhZGUwMWJiNGZhMmYyNWNmYjEwOGNjZWI4ODM0MDZkYWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI0MDc0MDg3MTgxOTIuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTgyNjkyMDI2OTQ5Nzk3ODc5NTkiLCJlbWFpbCI6Im5pbHMubW9sbGVyMjAwMEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkhXLUx0eW14enVJNnpYYVVvMkIyQXciLCJuYW1lIjoiTmlscyIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vYS9BQVRYQUp5YVVFcXVTMGJrWlZmNnJRN2JNNGw4ZGNaMFM5X0dVaVJZRU5aS0dRPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6Ik5pbHMiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTYyMTI1ODcwMCwiZXhwIjoxNjIxMjYyMzAwfQ.DjanpzA9Ws5AZC5PRnszuWcpXgyyHyPYh2B43jTmxCMEws_9ZiIzKn3xsMjuYcPfO6GwSWRdzYUHBTP0sUALCtlqtMMlRJhr6MF7cZ8Pr-hDvnEoc-mwroCdU6D_Td_CV4zzgvidCnzvvtuiY6hKzYb5mMcAtFyuiusES0HYjI3uapmeLRLmpNljWN4DFFQfLnq-RB6SkfKQYNhhXdU2xFsVyRccO5AZYYX3JWtKvCDRckyIsnxckjGnYkYlDAwnywex5WWKR3Jx6zR2dv6hb5kkx3DXy-e_CFApDXPdBtx_uB_39J5m3jvrFxq16Y1mrswuaw15k3Yt-VUCN7gjlQ"
                }
            };
            modelBuilder.Entity<User>().HasData(userData);

            var jobData = new[]
            {
                new
                {
                    JobId = Guid.Parse("debe96be-f90d-4677-8203-aba156b5ef74"),
                    ProviderId = userData[0].UserId,
                    Price = 5.32,
                    Title = "Business analyst",
                    Description = "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.\n\nNullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Expert,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                },
                new
                {
                    JobId = Guid.Parse("dba2cb60-c1db-4317-86e0-fe1b17c73b0a"),
                    ProviderId = userData[0].UserId,
                    Price = 27.12,
                    Title = "Photographer",
                    Description = "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.\n\nPhasellus in felis. Donec semper sapien a libero. Nam dui.\n\nProin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Intermediate,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                }, new
                {
                    JobId = Guid.Parse("410e6995-9b05-46e7-972b-15f34dc47b3e"),
                    ProviderId = userData[1].UserId,
                    Price = 70.00,
                    Title = "Plumming",
                    Description = "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.\n\nSed ante. Vivamus tortor. Duis mattis egestas metus.",
                    LevelOfExperience = LEVEL_OF_EXPERIENCE.Beginner,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now
                }
            };
            modelBuilder.Entity<Job>().HasData(jobData);

            var bookingData = new[]
            {
                new
                {
                    BookingId = Guid.Parse("ce1c8aa8-f0fd-473a-8ffc-c664d22f6a45"),
                    JobId = jobData[0].JobId,
                    ConsumerId = userData[6].UserId,
                    Address = "Rachelsmolen 1",
                    City = "Eindhoven",
                    Status = STATUS.Accepted,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 7, 2)
                },
                new
                {
                    BookingId = Guid.Parse("8303c61a-7f69-479d-a873-4494ad26d198"),
                    JobId = jobData[0].JobId,
                    ConsumerId = userData[6].UserId,
                    Address = "Rachelsmolen 22C",
                    City = "Eindhoven",
                    Status = STATUS.InProgress,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 9, 4)
                },
                new
                {
                    BookingId = Guid.Parse("730a982e-66e7-47fb-9de0-3333ce2d63d6"),
                    JobId = jobData[0].JobId,
                    ConsumerId = userData[5].UserId,
                    Address = "Rachelsmolen 92J",
                    City = "Eindhoven",
                    Status = STATUS.Denied,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2022, 2, 18)
                },
                new
                {
                    BookingId = Guid.Parse("e9f605e5-d616-45ea-ac97-df9d669faf8c"),
                    JobId = jobData[1].JobId,
                    ConsumerId = userData[6].UserId,
                    Address = "Rachelsmolen 10",
                    City = "Eindhoven",
                    Status = STATUS.Completed,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 5, 20)
                },
                new
                {
                    BookingId = Guid.Parse("caa53259-cbda-4fe7-b755-d99b2c435c02"),
                    JobId = jobData[1].JobId,
                    ConsumerId = userData[4].UserId,
                    Address = "Centerlane 2",
                    City = "Den Bosch",
                    Status = STATUS.Completed,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 6, 1)
                },
                new
                {
                    BookingId = Guid.Parse("d448d6a4-4256-4e43-b0e1-292695efca8f"),
                    JobId = jobData[1].JobId,
                    ConsumerId = userData[3].UserId,
                    Address = "Hoog catherijne",
                    City = "Utrecht",
                    Status = STATUS.InProgress,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 5, 28)
                },
                new
                {
                    BookingId = Guid.Parse("3f356b2e-a1f4-4d13-979e-0a13b26d1293"),
                    JobId = jobData[1].JobId,
                    ConsumerId = userData[3].UserId,
                    Address = "4480 Brentwood Drive",
                    City = "Austin",
                    Status = STATUS.Denied,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 7, 26)
                },
                new
                {
                    BookingId = Guid.Parse("ad6ca5df-8731-4d58-ab08-3698c561d3f7"),
                    JobId = jobData[2].JobId,
                    ConsumerId = userData[0].UserId,
                    Address = "22 Brickyard Drive",
                    City = "Yorktown Heights",
                    Status = STATUS.Denied,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 7, 2)
                },
                new
                {
                    BookingId = Guid.Parse("a6808daa-6562-4fd4-8e97-959ec89f6f50"),
                    JobId = jobData[2].JobId,
                    ConsumerId = userData[5].UserId,
                    Address = "66 West Euclid Court",
                    City = "Coatesville",
                    Status = STATUS.Pending,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 8, 14)
                },
                new
                {
                    BookingId = Guid.Parse("d6ce04e0-0cbd-4986-8522-875ced9bc05d"),
                    JobId = jobData[2].JobId,
                    ConsumerId = userData[6].UserId,
                    Address = "8939 Vale St.",
                    City = "Carrollton",
                    Status = STATUS.InProgress,
                    DateCreated = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    BookingDate = new DateTime(2021, 8, 6)
                }
            };
            modelBuilder.Entity<Booking>().HasData(bookingData);

            var reviewData = new[]
            {
                new
                {
                    ReviewId = Guid.Parse("649f29d1-f947-410e-a877-625f2c7cf3e1"),
                    ReviewerId = userData[6].UserId,
                    RevieweeId = userData[0].UserId,
                    Title = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    Description = "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.",
                    Rating = 4f,
                    Pros = "visualize seamless channels",
                    Cons = "engineer user-centric web-readiness",
                    DateCreated = DateTime.Now
                },
                new
                {
                    ReviewId = Guid.Parse("bd1effc0-c04e-4438-b44f-2c4005875f0d"),
                    ReviewerId = userData[6].UserId,
                    RevieweeId = userData[1].UserId,
                    Title = "In hac habitasse platea dictumst.",
                    Description = "Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.",
                    Rating = 1f,
                    Pros = "implement one-to-one e-business",
                    Cons = "integrate strategic initiatives",
                    DateCreated = DateTime.Now
                },
                new
                {
                    ReviewId = Guid.Parse("23ba2623-53b2-45cf-8493-a40cad3bf2f8"),
                    ReviewerId = userData[3].UserId,
                    RevieweeId = userData[0].UserId,
                    Title = "Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.",
                    Description = "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
                    Rating = 3f,
                    Pros = "transform cutting-edge networks",
                    Cons = "redefine compelling web services",
                    DateCreated = DateTime.Now
                }
            };
            modelBuilder.Entity<Review>().HasData(reviewData);
        }
    }
}
