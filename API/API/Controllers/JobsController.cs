﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using API.Attributes;

using Core.Controllers;
using Core.Db;
using Core.Models;

using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// This controller handles the interaction with jobs.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class JobsController : ControllerBase, IJobsController
    {
        private IJobDbHandler JobDbHandler { get; }
        private IUserDbHandler UserDbHandler { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="jobDbHandler">The database context for jobs.</param>
        /// <param name="userDbHandler">The database context for users.</param>
        public JobsController(IJobDbHandler jobDbHandler, IUserDbHandler userDbHandler)
        {
            this.JobDbHandler = jobDbHandler;
            this.UserDbHandler = userDbHandler;
        }

        // GET: Jobs
        /// <summary>
        /// Gets all the jobs.
        /// </summary>
        /// <returns>
        /// Ok() with the collection of all jobs. <br></br>
        /// </returns>
        [HttpGet]
        public async Task<ActionResult<List<Job>>> GetJobs()
        {
            return this.Ok(await this.JobDbHandler.GetJobsAsync());
        }

        // GET: Jobs/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get a job.
        /// </summary>
        /// <param name="id">The id of the job to find.</param>
        /// <returns>
        /// NotFound() if no job was found. <br></br> 
        /// Ok() with the job with matching id. <br></br> 
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Job>> GetJob(Guid id)
        {
            try
            {
                return this.Ok(await this.JobDbHandler.GetJobAsync(id));
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // GET: Jobs/user/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get all jobs for a specific user.
        /// </summary>
        /// <param name="userId">The id of the user providing the jobs to find.</param>
        /// <returns>
        /// Ok() with the list of jobs. 
        /// BadRequest() if the user does not exist.
        /// </returns>
        [HttpGet("user/{userId}")]
        public async Task<ActionResult<List<Job>>> GetJobsForUser(string userId)
        {
            try
            {
                await this.UserDbHandler.GetUserAsync(userId); // if the user doesnt exist
                return this.Ok((await this.JobDbHandler.GetJobsAsync()).Where(job => job.ProviderId == userId));
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        // POST: Jobs
        /// <summary>
        /// Creates a new job.
        /// </summary>
        /// <param name="job">The job to create.</param>
        /// <returns>
        /// Unauthorized() if the logged in user is not the job provider. <br></br>
        /// BadRequest() if the level of experience is not given. <br></br>
        /// BadRequest() if the provider does not exist. <br></br>
        /// CreatedAtAction() with the URI to the created job and the created job itself. <br></br> 
        /// </returns>
        /// <remarks>
        /// Required fields:<br></br>
        /// ProviderId<br></br>
        /// Price<br></br>
        /// Title<br></br>
        /// Description - optional<br></br>
        /// LevelOfExperience<br></br>
        /// </remarks>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<Job>> CreateJob(Job job)
        {
            try
            {
                if (((User)this.HttpContext.Items["User"]).UserId != job.ProviderId)
                    return this.Unauthorized();

                if (job.LevelOfExperience == Core.DataTypes.LEVEL_OF_EXPERIENCE.Empty)
                    return this.BadRequest("Level of experience is required.");
                await this.UserDbHandler.GetUserAsync(job.ProviderId); // if provider doesnt exist

                job.JobId = Guid.NewGuid();

                DateTime time = DateTime.Now;
                job.DateCreated = time;
                job.LastUpdated = time;

                await this.JobDbHandler.CreateJobAsync(job);
                return this.CreatedAtAction(nameof(GetJob), new { id = job.JobId }, job);
            }
            catch (ArgumentException e)
            {
                return this.BadRequest(e.Message);
            }
        }

        // PUT: Jobs/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Update a job.
        /// </summary>
        /// <param name="id">The id of the job to update.</param>
        /// <param name="job">The job to update.</param>
        /// <returns>
        /// Unauthorized() if the logged in user is not the job provider. <br></br>
        /// BadRequest() if the id does not match the job id. <br></br>
        /// BadRequest() if you are updating an illegal field. <br></br> 
        /// NoContent() if the update was successful. <br></br>
        /// NotFound() if the job does not exist. <br></br>
        /// </returns>
        /// <remarks>
        /// Fields available for update:<br></br>
        /// Price<br></br>
        /// Title<br></br>
        /// Description<br></br>
        /// LevelOfExperience<br></br>
        /// </remarks>
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult> UpdateJob(Guid id, Job job)
        {
            try
            {
                if (((User)this.HttpContext.Items["User"]).UserId != job.ProviderId)
                    return this.Unauthorized();

                if (id != job.JobId)
                    return this.BadRequest("Ids do not match.");

                Job existingJob = await this.JobDbHandler.GetJobAsync(id);
                if (existingJob.JobId != job.JobId || existingJob.ProviderId != job.ProviderId || existingJob.DateCreated != job.DateCreated)
                    return this.BadRequest("Cannot update job id, provider id or date created.");

                job.LastUpdated = DateTime.Now;

                await this.JobDbHandler.UpdateJobAsync(job);
                return this.NoContent();
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // DELETE: Jobs/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Deletes a job.
        /// </summary>
        /// <param name="id">The id of the job to delete.</param>
        /// <returns>
        /// Unauthorized() when the logged in user is not the job provider. <br></br>
        /// Ok() when the job is deleted. <br></br>
        /// NotFound() if the id does not exist. <br></br>
        /// </returns>
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteJob(Guid id)
        {
            try
            {
                Job job = await this.JobDbHandler.GetJobAsync(id);

                if (((User)this.HttpContext.Items["User"]).UserId != job.ProviderId)
                    return this.Unauthorized();

                await this.JobDbHandler.DeleteJobAsync(job.JobId);
                return this.Ok();
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}
