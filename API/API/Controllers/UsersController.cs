﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Controllers;
using Core.Db;
using Core.Models;
using Core.Wrappers;

using Google.Apis.Auth;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using static Google.Apis.Auth.GoogleJsonWebSignature;

using AuthorizeAttribute = API.Attributes.AuthorizeAttribute;

namespace API.Controllers
{
    /// <summary>
    /// This controller handles the interaction with users table.
    /// </summary>
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase, IUsersController
    {
        private IValidateAsyncWrapper validateAsyncWrapper { get; }
        private IConfiguration Config { get; }
        private IUserDbHandler UserDbHandler { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="userDbHandler">The database handler for users.</param>
        /// <param name="validateAsyncWrapper">The ValidateAsync wrapper.</param>
        public UsersController(IConfiguration config, IUserDbHandler userDbHandler, IValidateAsyncWrapper validateAsyncWrapper)
        {
            this.Config = config;
            this.UserDbHandler = userDbHandler;
            this.validateAsyncWrapper = validateAsyncWrapper;
        }

        // GET: Users
        /// <summary>
        /// Gets all the users.
        /// </summary>
        /// <returns>
        /// Ok() with the collection of all users. <br></br>
        /// </returns>
        [HttpGet]
        public async Task<ActionResult<List<User>>> GetUsers()
        {
            return this.Ok(await this.UserDbHandler.GetUsersAsync());
        }

        // GET: Users/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Get a user.
        /// </summary>
        /// <param name="id">The id of the user to find.</param>
        /// <returns>
        /// Ok() with the user with matching id. <br></br> 
        /// NotFound() if no user was found. <br></br>
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(string id)
        {
            try
            {
                return this.Ok(await this.UserDbHandler.GetUserAsync(id));
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        // DELETE: Users/00000000-0000-0000-0000-000000000000
        /// <summary>
        /// Deletes a user.
        /// </summary>
        /// <param name="id">The id of the user to delete.</param>
        /// <returns>
        /// Ok() when the user is deleted. <br></br> 
        /// NotFound() if the id does not exist. <br></br>
        /// </returns>
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult> DeleteUser(string id)
        {
            try
            {
                if (((User)this.HttpContext.Items["User"]).UserId != id)
                    return this.Unauthorized();

                await this.UserDbHandler.DeleteUserAsync(id);
                return this.Ok();
            }
            catch (ArgumentException e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Registers a new google user in our system.
        /// </summary>
        /// <param name="user">The extra user info.</param>
        /// <returns>
        /// BadRequest() if the token is missing. <br></br>
        /// Unauthorized() if the token is invalid. <br></br>
        /// BadRequest() if a user with the given id already exists. <br></br>
        /// CreatedAtAction() if registration was successful. <br></br>
        /// </returns>
        /// <remarks>
        /// Required fields:<br></br>
        /// Email<br></br>
        /// FirstName<br></br>
        /// LastName<br></br>
        /// DateOfBirth<br></br>
        /// ProfilePictureLink - optional<br></br>
        /// Phone<br></br>
        /// Address<br></br>
        /// City<br></br>
        /// </remarks>
        [HttpPost("auth/google/register")]
        public async Task<ActionResult> Register(User user)
        {
            try
            {
                string token = this.HttpContext.Request.Headers["Authorization"];
                if (token == null || token.Trim() == "")
                    return this.BadRequest("No token.");

                Payload googleUser = await this.validateAsyncWrapper.ValidateAsync(token);

                user.UserId = googleUser.Subject;
                user.Rating = -1f;
                user.DateCreated = DateTime.Now;
                user.LastLogin = DateTime.Now;

                try
                {
                    await this.UserDbHandler.GetUserAsync(user.UserId);
                    return this.BadRequest("User with that id already exists.");
                }
                catch (ArgumentException)
                {
                    // continue with the method
                }

                await this.UserDbHandler.CreateUserAsync(user);

                return this.CreatedAtAction(nameof(GetUser), new { id = user.UserId }, user);
            }
            catch (InvalidJwtException)
            {
                return this.Unauthorized("Invalid token.");
            }
        }
    }
}
