﻿using System;
using System.Reflection;
using System.Threading.Tasks;

using API.Helpers;
using API.Attributes;
using API.Controllers;

using Core.Db;
using Core.Models;
using Core.Wrappers;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Moq;

using Xunit;
using System.Collections.Generic;
using System.Net.Http;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using Google.Apis.Auth;

namespace APITests.Controllers
{
    public class UsersControllerTests : IDisposable
    {
        public Mock<IUserDbHandler> _userMock;
        public Mock<IValidateAsyncWrapper> _validateAsyncWrapperMock;
        UsersController _controller;

        // setup
        public UsersControllerTests()
        {
            _userMock = new Mock<IUserDbHandler>();
            _validateAsyncWrapperMock = new Mock<IValidateAsyncWrapper>();
        }

        // breakdown

        public void Dispose()
        {

        }

        #region Get

        [Fact]
        public async Task GetUsers_ReturnsOkResult()
        {
            //Arrange
            this._userMock.Setup(p => p.GetUsersAsync());
            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);

            // Act
            var okResult = await this._controller.GetUsers();

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public async Task GetUser_ReturnsUser_WhenMatchingIDExists()
        {
            //Arrange
            string uID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            User userDto = new User
            {
                UserId = uID
            };
            this._userMock.Setup(p => p.GetUserAsync(uID)).ReturnsAsync(userDto);
            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);

            //Act
            ActionResult<User> response = await this._controller.GetUser(uID);

            //Assert
            OkObjectResult result = response.Result as OkObjectResult;
            User jobResponse = result.Value as User;

            Assert.Equal(userDto, jobResponse);
        }

        [Fact]
        public async Task GetUser_WhenNoUserFound_ReturnsNotFound()
        {
            //Arrange
            string uID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            this._userMock.Setup(p => p.GetUserAsync(uID)).ThrowsAsync(new ArgumentException());
            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);

            //Act
            ActionResult<User> response = await this._controller.GetUser(uID);

            //Assert
            Assert.IsType<NotFoundObjectResult>(response.Result);
        }

        #endregion
        #region Delete

        [Fact]
        public async Task DeleteUser_ReturnsOkResult()
        {
            // Arrange

            string userId = "110811874816751097523";

            this._userMock.Setup(p => p.GetUserAsync(userId)).ReturnsAsync(new User { UserId = userId });
            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = userId });

            // Act
            ActionResult okResponse = await this._controller.DeleteUser(userId);

            // Assert
            Assert.IsType<OkResult>(okResponse);
        }

        [Fact]
        public async Task DeleteUser_WhenIdNotFound_ReturnsNotFound()
        {
            // Arrange
            string userId = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            this._userMock.Setup(p => p.DeleteUserAsync(userId, true)).ThrowsAsync(new ArgumentException());
            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = userId });

            // Act
            var response = await this._controller.DeleteUser(userId);

            // Assert
            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        public async Task DeleteUser_WhenWrongHttpContext_ReturnsUnauthorizedResult()
        {
            // Arrange
            string userId = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            this._userMock.Setup(p => p.GetUserAsync(userId)).ReturnsAsync(new User { UserId = userId });
            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);

            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Items.Add("User", new User { UserId = GoogleUserIdGenerator.GenerateRandomGoogleUserId() });

            // Act
            ActionResult okResponse = await this._controller.DeleteUser(userId);

            // Assert
            Assert.IsType<UnauthorizedResult>(okResponse);
        }



        #endregion
        #region Register

        [Fact]
        public async Task RegisterUser_ReturnsCreatedAtAction()
        {
            //Arrange
            string userID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            User userDto = new User
            {
                Email = "flame@imgur.com",
                FirstName = "Frar",
                LastName = "Laine",
                DateOfBirth = new DateTime(2009, 5, 2),
                ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                Phone = "(976) 5136010",
                Address = "9 Golden Leaf city",
                City = "Balayinga"
            };

            this._userMock.Setup(p => p.GetUserAsync(userID)).ThrowsAsync(new ArgumentException());
            this._validateAsyncWrapperMock.Setup(k => k.ValidateAsync(It.IsAny<String>(), null, false))
                .ReturnsAsync(new Payload { Subject = userID });

            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Request.Headers["Authorization"] = "story";


            //Act
            var response = await this._controller.Register(userDto);

            //Assert

            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public async Task RegisterUser_WhenMissingToken_ReturnsBadRequest()
        {
            //Arrange
            string userID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            User userDto = new User
            {
                Email = "flame@imgur.com",
                FirstName = "Frar",
                LastName = "Laine",
                DateOfBirth = new DateTime(2009, 5, 2),
                ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                Phone = "(976) 5136010",
                Address = "9 Golden Leaf city",
                City = "Balayinga"
            };

            this._userMock.Setup(p => p.GetUserAsync(userID)).ThrowsAsync(new ArgumentException());
            this._validateAsyncWrapperMock.Setup(k => k.ValidateAsync(It.IsAny<String>(), null, false))
                .ReturnsAsync(new Payload { Subject = userID });

            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Request.Headers["Authorization"] = "";


            //Act
            var response = await this._controller.Register(userDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        [Fact]
        public async Task RegisterUser_WhenUnauthorizedToken_ReturnsUnauthorized()
        {
            //Arrange
            string userID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            User userDto = new User
            {
                Email = "flame@imgur.com",
                FirstName = "Frar",
                LastName = "Laine",
                DateOfBirth = new DateTime(2009, 5, 2),
                ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                Phone = "(976) 5136010",
                Address = "9 Golden Leaf city",
                City = "Balayinga"
            };

            this._userMock.Setup(p => p.GetUserAsync(userID)).ThrowsAsync(new InvalidJwtException("Invalid token."));
            this._validateAsyncWrapperMock.Setup(k => k.ValidateAsync(It.IsAny<String>(), null, false))
                .ReturnsAsync(new Payload { Subject = userID });

            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Request.Headers["Authorization"] = "story";


            //Act
            var response = await this._controller.Register(userDto);

            //Assert

            Assert.IsType<UnauthorizedObjectResult>(response);
        }

        [Fact]
        public async Task RegisterUser_WhenIdAlreadyExists_ReturnsBadRequest()
        {
            //Arrange
            string userID = GoogleUserIdGenerator.GenerateRandomGoogleUserId();

            User userDto = new User
            {
                UserId = userID,
                Email = "flame@imgur.com",
                FirstName = "Frar",
                LastName = "Laine",
                DateOfBirth = new DateTime(2009, 5, 2),
                ProfilePictureLink = "http://dummyimage.com/180x180.png/5fa2dd/ffffff",
                Phone = "(976) 5136010",
                Address = "9 Golden Leaf city",
                City = "Balayinga"
            };

            this._userMock.Setup(p => p.GetUserAsync(userID)).ReturnsAsync(new User{UserId = userDto.UserId });
            this._validateAsyncWrapperMock.Setup(k => k.ValidateAsync(It.IsAny<String>(), null, false))
                .ReturnsAsync(new Payload { Subject = userID });

            this._controller = new UsersController(new Mock<IConfiguration>().Object, this._userMock.Object, this._validateAsyncWrapperMock.Object);
            this._controller.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            this._controller.HttpContext.Request.Headers["Authorization"] = "story";


            //Act
            var response = await this._controller.Register(userDto);

            //Assert

            Assert.IsType<BadRequestObjectResult>(response);
        }

        #endregion

        [Fact]
        public async Task DeleteUser_MustHaveAuthorizeAttribute()
        {
            // Arrange
            MethodBase method = typeof(UsersController).GetMethod(nameof(this._controller.DeleteUser));

            // Act
            object[] attributes = method.GetCustomAttributes(typeof(AuthorizeAttribute), true);

            // Assert
            Assert.Single(attributes);
        }
    }
}
