﻿using System.Threading.Tasks;

using Google.Apis.Auth;
using Google.Apis.Util;

using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace Core.Wrappers
{
    /// <summary>
    /// This interface defines the wrapper for the static Google.Apis.Auth.GoogleJsonWebSignature.ValidateAsync(string token)
    /// </summary>
    public interface IValidateAsyncWrapper
    {
        /// <summary>
        /// Validates a Google-issued Json Web Token (JWT). <br></br>
        /// Will throw a Google.Apis.Auth.InvalidJwtException if the passed value is not valid JWT signed by Google.
        /// </summary>
        /// <param name="jwt">The JWT to validate.</param>
        /// <param name="clock">
        /// Optional. <br></br>
        /// The Google.Apis.Util.IClock to use for JWT expiration verification. <br></br>
        /// Defaults to the system clock.
        /// </param>
        /// <param name="forceGoogleCertRefresh">
        /// Optional. <br></br>
        /// If true forces new certificates to be downloaded from Google. <br></br> 
        /// Defaults to false.
        /// </param>
        /// <returns>The JWT payload, if the JWT is valid. Throws a <see cref="InvalidJwtException"/> otherwise.</returns>
        /// <exception cref="InvalidJwtException">Throws when passed a JWT that is not a valid JWT signed by Google.</exception>
        /// <remarks>
        /// Follows the procedure to validate a JWT ID token. <br></br>
        /// Google certificates are cached, and refreshed once per hour. This can be overridden by setting forceGoogleCertRefresh to true.
        /// </remarks>
        Task<Payload> ValidateAsync(string jwt, IClock clock = null, bool forceGoogleCertRefresh = false);
    }
}
