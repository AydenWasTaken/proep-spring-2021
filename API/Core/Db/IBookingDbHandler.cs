﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

namespace Core.Db
{
    /// <summary>
    /// This interface defines the methods for the booking context.
    /// </summary>
    public interface IBookingDbHandler
    {
        #region Async
        /// <summary>
        /// Asynchronously get all bookings.
        /// </summary>
        /// <returns>The collection of all bookings.</returns>
        Task<List<Booking>> GetBookingsAsync();

        /// <summary>
        /// Asynchronously get a booking.
        /// </summary>
        /// <param name="id">The id of the booking to get.</param>
        /// <returns>The booking with the given id.</returns>
        /// <exception cref="ArgumentException">Throws when no booking with the id could be found.</exception>
        Task<Booking> GetBookingAsync(Guid id);

        /// <summary>
        /// Asynchronously create a new booking.
        /// </summary>
        /// <param name="booking">The booking object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        Task CreateBookingAsync(Booking booking, bool saveChanges = true);

        /// <summary>
        /// Asynchronously update a booking.
        /// </summary>
        /// <param name="booking">The booking with updated values.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no booking with the id could be found.</exception>
        Task UpdateBookingAsync(Booking booking, bool saveChanges = true);

        /// <summary>
        /// Asynchronously delete a booking.
        /// </summary>
        /// <param name="id">The id of the booking to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no booking with the id could be found.</exception>
        Task DeleteBookingAsync(Guid id, bool saveChanges = true);
        #endregion

        #region Sync
        /// <summary>
        /// Get all bookings.
        /// </summary>
        /// <returns>The collection of all bookings.</returns>
        List<Booking> GetBookings();

        /// <summary>
        /// Get a booking.
        /// </summary>
        /// <param name="id">The id of the booking to get.</param>
        /// <returns>The booking with the given id.</returns>
        /// <exception cref="ArgumentException">Throws when no booking with the id could be found.</exception>
        Booking GetBooking(Guid id);

        /// <summary>
        /// Create a new booking.
        /// </summary>
        /// <param name="booking">The booking object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        void CreateBooking(Booking booking, bool saveChanges = true);

        /// <summary>
        /// Update a booking.
        /// </summary>
        /// <param name="booking">The booking with updated values.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no booking with the id could be found.</exception>
        void UpdateBooking(Booking booking, bool saveChanges = true);

        /// <summary>
        /// Delete a booking.
        /// </summary>
        /// <param name="id">The id of the booking to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no booking with the id could be found.</exception>
        void DeleteBooking(Guid id, bool saveChanges = true);
        #endregion
    }
}
