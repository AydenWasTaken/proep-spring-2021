﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Models;

namespace Core.Db
{
    /// <summary>
    /// This interface defines the methods for the user context.
    /// </summary>
    public interface IUserDbHandler
    {
        #region Async
        /// <summary>
        /// Get all users.
        /// </summary>
        /// <returns>The collection of all users.</returns>
        Task<List<User>> GetUsersAsync();

        /// <summary>
        /// Get a user.
        /// </summary>
        /// <param name="id">The id of the user to get.</param>
        /// <returns>The user with the given id.</returns>
        /// <exception cref="ArgumentException">Throws when no user with the id could be found.</exception>
        Task<User> GetUserAsync(string id);

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user">The user object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        Task CreateUserAsync(User user, bool saveChanges = true);

        /// <summary>
        /// Update a user.
        /// </summary>
        /// <param name="user">The user with updated values.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no user with the id could be found.</exception>
        Task UpdateUserAsync(User user, bool saveChanges = true);

        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <param name="id">The id of the user to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no user with the id could be found.</exception>
        Task DeleteUserAsync(string id, bool saveChanges = true);
        #endregion

        #region Sync
        /// <summary>
        /// Get all users.
        /// </summary>
        /// <returns>The collection of users currently stored.</returns>
        List<User> GetUsers();

        /// <summary>
        /// Get a user.
        /// </summary>
        /// <param name="id">The id of the user to get.</param>
        /// <returns>The user with the given id.</returns>
        /// <exception cref="ArgumentException">Throws when no user with the id could be found.</exception>
        User GetUser(string id);

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user">The user object to create.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        void CreateUser(User user, bool saveChanges = true);

        /// <summary>
        /// Update a user.
        /// </summary>
        /// <param name="user">The user with updated values.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no user with the id could be found.</exception>
        void UpdateUser(User user, bool saveChanges = true);

        /// <summary>
        /// Delete a user.
        /// </summary>
        /// <param name="id">The id of the user to delete.</param>
        /// <param name="saveChanges">Whether this method should save its changes or not. It is rare that this needs to be false.</param>
        /// <exception cref="ArgumentException">Throws when no user with the id could be found.</exception>
        void DeleteUser(string id, bool saveChanges = true);
        #endregion
    }
}
