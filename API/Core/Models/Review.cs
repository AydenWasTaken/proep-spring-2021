﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Core.Models
{
    /// <summary>
    /// This class describes how a single review looks. 
    /// Every user can be given many reviews. 
    /// Every user can post many reviews.
    /// </summary>
    public class Review : IEquatable<Review>
    {
        /// <summary>
        /// The id of this review.
        /// </summary>
        [Key]
        [ExcludeFromCodeCoverage]
        public Guid ReviewId { get; set; }

        /// <summary>
        /// The id of the user that wrote this review.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string ReviewerId { get; set; }

        /// <summary>
        /// The id of the user that this review is about.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string RevieweeId { get; set; }

        /// <summary>
        /// The title of this review.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public string Title { get; set; }

        /// <summary>
        /// The description of this review.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string Description { get; set; }

        /// <summary>
        /// The rating the reviewer gave the reviewee.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public float Rating { get; set; }

        /// <summary>
        /// The pros of working with the reviewee.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string Pros { get; set; }

        /// <summary>
        /// The cons of working with the reviewee.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public string Cons { get; set; }

        /// <summary>
        /// The date at which this review is created.
        /// </summary>
        [Required]
        [ExcludeFromCodeCoverage]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="obj">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as Review);
        }

        /// <summary>
        /// Checks whether an object is equal to this object.
        /// </summary>
        /// <param name="other">The object to compare this object to.</param>
        /// <returns>True if the ids match. False otherwise.</returns>
        public bool Equals(Review other)
        {
            return other != null &&
                   this.ReviewId.Equals(other.ReviewId);
        }

        /// <summary>
        /// Generates the hash code for this object using the id.
        /// </summary>
        /// <returns>The generated hash code.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.ReviewId);
        }

        /// <summary>
        /// Checks whether left is equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are equal. False otherwise.</returns>
        public static bool operator ==(Review left, Review right)
        {
            return EqualityComparer<Review>.Default.Equals(left, right);
        }

        /// <summary>
        /// Checks whether left is not equal to right based on id.
        /// </summary>
        /// <param name="left">The left side of the operator to evaluate.</param>
        /// <param name="right">The right side of the operator to evaluate.</param>
        /// <returns>True if ids are not equal. False otherwise.</returns>
        public static bool operator !=(Review left, Review right)
        {
            return !(left == right);
        }
    }
}
